+++
Categories = []
Description = "Orchestrating cloud deployments with Kubernetes and Docker"
Keywords = []
Tags = []
date = "2016-08-31T14:03:27-07:00"
title = "Cloud Orchestration"
weight = 20
+++


Blue-Green deployments, rolling updates, and federated clusters? Take
control with Kubernetes. Sarum Technologies can install and configure
a Kubernetes control plane for your applications on AWS, Azure or
bare-metal data centers, and assist your development team in getting
the maximum benefit from the abstractions provided.
