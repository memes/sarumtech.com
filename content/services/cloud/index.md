+++
Categories = []
Description = "Building a cloud architecture for Amazon Web Services, Google Cloud Platform or Microsoft Azure with Kubernetes and Docker"
Keywords = []
Tags = []
date = "2016-09-02T13:58:59-07:00"
title = "Cloud Architecture"
weight = 10

+++
There's more to cloud migration than simply deploying an application
to a rented virtual computer, and Sarum Technologies consultants know
this. A scalable and reliable application requires planning and the
benefit of experience.

Amazon Web Services, Microsoft Azure and Google Cloud Platform have
subtle and distinct options that may make one a better fit for your
business needs than the others. We can help you make the right choice.
