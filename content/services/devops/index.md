+++
Categories = []
Description = "DevOps is more than just a buzzword"
Keywords = []
Tags = []
date = "2016-08-31T20:10:38-07:00"
title = "DevOps"
weight = 50
+++

Companies are embracing [DevOps](//en.wikipedia.org/wiki/DevOps)
practices for development projects, but many lack the experience to
assemble a development pipeline from scratch. Sarum Technologies can
assist your team, or build a custom pipeline to suit your needs.
