+++
Categories = []
Description = "About Sarum Technologies"
Keywords = []
Tags = []
date = "2016-08-30T12:37:00-07:00"
title = "About"

+++
Sarum Technologies provides expertise and consulting for
enterprises using or moving to public cloud services from Amazon,
Microsoft and Google. With many years of experience in cloud
architecture and development, Sarum Technologies can help at any stage of your
implementation process.
